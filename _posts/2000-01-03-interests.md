---
title: "Interests"
bg: "#005fb7"
color: white
fa-icon: heart
---

### Languages

My current plans are 

- improve my Mandarin Chinese (普通话 -- simplified characters) and achieve HSK2 and HSK3 in 2019
- reach A1 level in Russian in 2019

### Education

I am researching on how to teach programming to young kids:

- Age 4-6: teaching programming with no screens
- Age 6 - 7: block programming and UCB Logo.


