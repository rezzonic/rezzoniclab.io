---
title: "social"
bg: '#003b72'
color: white
fa-icon: comments
style: center
---


<span class="more-icons">
<a href="https://www.linkedin.com/in/vrezzoni/"><i class="fa fa-stack fa-linkedin fa-3x"></i></a>
<a href="https://www.facebook.com/rezzonic"><i class="fa fa-stack fa-facebook-square fa-3x"></i></a>
<a href="https://twitter.com/ViRezzonico"><i class="fa fa-stack fa-twitter fa-3x"></i></a>
<a href="https://www.duolingo.com/Vicky118915"><i class="fa fa-stack fa-3x" style="font-size: 25px; background: url(../img/duo-white.png) no-repeat 0 0px;"></i></a>
</span>


